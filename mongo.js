const mongoose = require('mongoose')

// Replace with the URL of your own database. Do not store the password on GitLab!
const url = 'mongodb+srv://pjsabe:<Password>@mycluster-dhazr.mongodb.net/MyDatabase'

mongoose.connect(url)

const Person = mongoose.model('Person', {
  name: String,
  number: String
})

const person = new Person({
    name: process.argv[2],
    number: process.argv[3]
  })

let name = process.argv[2]
let number = process.argv[3]

if (name === undefined || number === undefined){
    Person
  .find({})
  .then(result => {
    result.forEach(person => {
      console.log(person)
      mongoose.connection.close()
    })
    
  })
} else {

    person
    .save()
    .then(response => {
        console.log(`adding person ${name} number ${number} to the directory`)
        mongoose.connection.close()
    })
    }
